<?php

namespace Demo;

use SimpleUser\User as BaseUser;

class User extends BaseUser
{
    public function __construct($email)
    {
        parent::__construct($email);
    }

    public function getTwitterUsername()
    {
        return $this->getCustomField('twitterUsername');
    }

    public function setTwitterUsername($twitterUsername)
    {
        $this->setCustomField('twitterUsername', $twitterUsername);
    }
	
	public function getUsrPhone()
    {
        return $this->getCustomField('usrPhone');
    }

    public function setUsrPhone($usrPhone)
    {
        $this->setCustomField('usrPhone', $usrPhone);
    }

    public function getStatus(){
		return $this->getCustomField('currentStatus');
	}
	
	public function setStatus($status){
		$this->setCustomField('currentStatus', $status);
	}
	
	public function validate()
    {
        $errors = parent::validate();

        if ($this->getTwitterUsername() && strpos($this->getTwitterUsername(), '@') !== 0) {
            $errors['twitterUsername'] = 'Twitter username must begin with @.';
        }

        return $errors;
    }
}
