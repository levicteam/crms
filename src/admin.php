<?php

$app->get('/admin', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}
	return $app['twig']->render('index.twig', array(
									'zals'=>$depts,
									'zal'=>$current_dept,
									'mycash'=>$mycash,
									'iamactive'=>true,
									'systems'=>$systems,
									'cashiers'=>$dept_cashiers));
});

$app->get('/admin/regions', function() use ($app) {
	$data['tree']=Regions::getFullTree();
	//var_dump(Regions::getMyDepts(3));
	return $app['twig']->render('region-list.twig', $data);
});

$app->get('/admin/regions/view-{id}', function($id) use ($app) {
	$data=Regions::getSingle($id);

	return $app['twig']->render('region-view.twig', $data);
});

$app->get('/admin/regions/addchild/{root}', function($root) use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}
	$data['action']="/index.php/admin/regions/addchild";
	$data['regions']=Regions::getParent($root);
	$data['managers']=UsrDept::getFreeUsers("ROLE_MANAGER");
	//$data['managers']=UsrDept::getUsersOfRegion('ROLE_MANAGER',$user['id'],false,true);
	return $app['twig']->render('region-new.twig', $data);
});

$app->get('/admin/exp/list', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}
	$data['types']=Cash::getTypeExps();
	return $app['twig']->render('explist.twig', $data);
});

$app->get('/admin/exp/new', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}
	$data['action']="/index.php/admin/exp/new";
	$data['id']="";
	$data['name']="";
	$data['desc']="";
	return $app['twig']->render('expedit-form.twig', $data);
});

$app->get('/admin/exp/edit-{eid}', function($eid) use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}
	$data['action']="/index.php/admin/exp/edit";
	$t=Cash::getExpById($eid);
	
	$data['id']=$eid;
	$data['name']=$t["expenses"];
	$data['desc']=$t['expdesc'];
	return $app['twig']->render('expedit-form.twig', $data);
});

$app->get('/admin/sys/edit-{eid}', function($eid) use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}
	$data['action']="/index.php/admin/sys/edit";
	$t=Systems::getSys($eid);
	
	$data['id']=$eid;
	$data['name']=$t["name"];
	$data['desc']=$t['desc'];
	return $app['twig']->render('expedit-form.twig', $data);
});


$app->post('/admin/exp/new', function() use ($app){
	$message = $app['request'];
	$form['name']=$message->get('name');
	$form['desc']=$message->get('desc');
	
	if ($lastId=Cash::pushExpType($form)) 
		return $app->redirect('/index.php/admin/exp/list');
	return "error";
		
});

$app->post('/admin/exp/edit', function() use ($app){
	$message = $app['request'];
	$form['id']=$message->get('id');
	$form['name']=$message->get('name');
	$form['desc']=$message->get('desc');
	
	Cash::updateExpType($form);
		return $app->redirect('/index.php/admin/exp/list');
	return "error";
		
});


$app->get('/admin/sys/list', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}
	$data['systems']=Systems::getListSys();
	return $app['twig']->render('sys-list.twig', $data);
});

$app->get('/admin/sys/new', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}
	$data['action']="/index.php/admin/sys/new";
		$data['id']="";
	$data['name']="";
	$data['desc']="";
	return $app['twig']->render('sys-edit-form.twig', $data);
});


$app->post('/admin/sys/new', function() use ($app){
	$message = $app['request'];
	$form['name']=$message->get('name');
	$form['desc']=$message->get('desc');
	Systems::pushSys($form);
		return $app->redirect('/index.php/admin/sys/list');

});



$app->post('/admin/sys/edit', function() use ($app){
	$message = $app['request'];
	$form['id']=$message->get('id');
	$form['name']=$message->get('name');
	$form['desc']=$message->get('desc');
	
	Systems::updateSys($form);
		return $app->redirect('/index.php/admin/sys/list');
	return "error";
		
});




$app->post('/admin/regions/addchild', function() use ($app){
	$message = $app['request'];
	$form['name']=$message->get('addr');
	$form['parent']=$message->get('reg');
	$form['manager']=$message->get('manager');
	if ($lastId=Regions::pushReg($form)) 
		return $app->redirect('/index.php/admin/regions/view-'.$lastId);
	return "error";
		
});
