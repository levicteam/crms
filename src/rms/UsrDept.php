﻿<?php


class UsrDept 
{

	public static function startConnection(){
		try {
		$dbo = new PDO("mysql:host=mysql.hostinger.co.uk;dbname=u462245891_rms", 
		"u462245891_admin", '10pan050r0ga');
		
		return $dbo;
		} catch (PDOException $e) {
		return false;
		}
	}
	
	/*
	public static function getLocalUsers(){
		if ($db=UsrDept::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT * FROM users WHERE roles='ROLE_LOCALUSER'";
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}
		return false;
	}
	
	public static function getLocalManagers(){
		if ($db=UsrDept::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT * FROM users WHERE roles='ROLE_MANAGER'";
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}
		return false;	
	}
*/
	public static function getDepartments(){
		if ($db=UsrDept::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			//$qry="SELECT ";
			
			$qry = "SELECT * FROM department";
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}
		return false;	
	}
			
	//================================
	public static function getUsersOfDepartment($role='',$me=0,$dpt=false){
			if (!$dpt)
			{
				$dpt=UsrDept::getMyActiveDept($me);
				//////var_dump($me);
				$dpt=$dpt['id'];
			}
			if ($db=UsrDept::startConnection())
			{
			$db->exec("SET time_zone = '+02:00';");

			
			$qry = "SELECT users.* FROM 
					users INNER JOIN usrreg 
					ON users.id = usrreg.user 
					WHERE users.roles LIKE '%$role%' AND usrreg.region=".$dpt." AND users.id != $me";
					//////var_dump($qry);
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}
		return -1;		
	}
	
	public static function getUsersOfRegion($role='ROLE_LOCALUSER',$me=0,$reg=false,$free=false){
		if (!$reg)
			{
				$reg=UsrDept::getMyActiveRegion($me);
				
			}
		if ($free)
			$free=" AND usrreg.active = 0 ";
		else $free="";
		if ($db=UsrDept::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");

			
			$qry = "SELECT users.*, usrreg.active as active FROM 
					users INNER JOIN usrreg 
					ON users.id = usrreg.user 
					WHERE users.roles LIKE '%$role%' AND usrreg.region=".$reg." AND users.id != $me".$free;
					//var_dump($qry);
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}
		return false;		
	}
	
	public static function getActiveManager($reg=0){
		if ($db=UsrDept::startConnection())
		{
			
			
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT users.* FROM users INNER JOIN usrreg 
					ON users.id = usrreg.user 
					WHERE usrreg.region = $reg AND users.roles LIKE '%ROLE_MANAGER%'";
					////var_dump($qry);
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult=$result;
			$db=null;
			return $arResult;
		}
		return -1;		
	}
	
	public static function getActiveCashier($dept=0, $role="ROLE_LOCALUSER"){
		if ($db=UsrDept::startConnection())
		{
			
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT users.* FROM users INNER JOIN usrreg 
					ON users.id = usrreg.user 
					WHERE usrreg.region = $dept AND usrreg.active = 1 AND users.roles LIKE '%$role%'
						";
// var_dump($qry);
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult=$result;
			$db=null;
			return $arResult;
		}
		return -1;		
	}
	
	
	public static function getRegByDept($dept=0){
		if ($db=UsrDept::startConnection())
		{
			
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT * FROM department 
					WHERE department.id = $dept ";
 //var_dump($qry);
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult=$result['region'];
			$db=null;
			return $arResult;
		}
		return -1;		
	}
	
		
	
		public static function getActivePov(){
		if ($db=UsrDept::startConnection())
		{
			
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT * FROM users INNER JOIN manager 
					ON users.id = manager.userid WHERE ispov = 1";
			//////var_dump($qry);
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}
		return false;		
	}
	
	
	
	public static function getMyActiveDept($usr){
		if ($db=UsrDept::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			/*$qry = "SELECT department.* FROM usrdpt INNER JOIN department ON usrdpt.dept = department.id
					WHERE usrdpt.user = $usr AND usrdpt.active = 1";
					*/
			$qry = "SELECT department.* FROM  department WHERE department.active_cashier = $usr";
				
					//////var_dump($qry);
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult=$result;
			$db=null;
			return $arResult;
		}
		return -1;			
	}
	
	public static function getMyActiveRegion($usr){
		if ($db=UsrDept::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			//$qry = "SELECT regions.* FROM usrreg INNER JOIN department ON usrreg.region = department.region
			//		WHERE usrdpt.user = $usr AND usrdpt.active = 1";
					
			//$qry = "SELECT department.* FROM  department WHERE department.active_cashier = $usr";
			$qry = "SELECT * FROM  usrreg INNER JOIN users ON usrreg.user = users.id WHERE usrreg.active = 1 AND usrreg.user = $usr";
				
					//////var_dump($qry);
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult=$result['region'];
			$db=null; 
			return $arResult;
		}
		return false;			
	}
	
	public static function getFullDepartment($dept=0){
		$outResult['active_cashier']=UsrDept::getActiveCashier($dept);
		if ($db=UsrDept::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT * FROM department WHERE id = $dept";
			$res=$db->query($qry);
			if ($dept!=0)
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$outResult=$arResult[0];
			$qry = "SELECT users.* FROM users WHERE id = ".$outResult['active_cashier'];
			$res=$db->query($qry);
			$arResult=array();
			if ($dept!=0)
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult=$result;
			$db=null;
			$outResult['active_cashier']=$arResult;
			
			return $outResult;
		}
		return false;		
	}
	
	public static function iamActive($uid){
		if ($db=UsrDept::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT * FROM usrreg 
					WHERE active = 1 AND user = $uid";
					
					////var_dump($qry);
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult=$result;
			$db=null;
			return ($arResult)?true:false;
		}
		return false;		
	}
	
	public static function iamBlocked($uid){
		if ($db=UsrDept::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT * FROM usrreg 
					WHERE usrreg.user = $uid AND usrreg.blocked = 1";
					//////var_dump($qry);
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult=$result;
			$db=null;
			return ($arResult)?true:false;
		}
		return false;		
	}
	

	public static function pushDept($form){
		
		if ($dbh=UsrDept::startConnection())
		{
			try
			{ 
			$dbh -> beginTransaction (); 
			$dbh->exec("SET time_zone = '+02:00';");
			$qry = "INSERT INTO `department` (`current_cash`, `addr`, `region`, `active_cashier`, `description`, `sysexchblock`) 
			VALUES ('".
					$form['cash']."','".
					$form['addr']."','".
					$form['region']."','".
					$form['cashier']."','".
					$form['desc']."','".
					$form['sysexchblock']."')";
			////var_dump($form);
			
			$dbh->exec($qry);
			$lastId=$dbh->lastInsertId();
			$qry="UPDATE usrreg SET active = 1 WHERE region  = ".$form['region'].
				" AND user = ".$form['cashier'];
				
			$dbh->exec($qry);
			
						foreach ($form['systems'] as $item){
				$qry="INSERT INTO sysdpt (`sys`,`dept`,`current_cash`) VALUES ('$item','$lastId','0')";
			$res=$dbh->exec($qry);	
			}
			
			$dbh->commit();
			}
			catch ( Exception $e )
			{ 
				$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
			} 
			$dbh=null;
		}
		return 1;
		return $lastId;
	}

	public static function getFreeUsers($role="ROLE_LOCALUSER"){
		if ($db=UsrDept::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT * FROM usrreg RIGHT OUTER JOIN `users` ON usrreg.user=users.id 
WHERE usrreg.user is null AND users.roles LIKE '%$role%'";
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}
		return false;
	}	

	public static function setCashier($user,$reg){
		
		if ($dbh=UsrDept::startConnection())
		{
			try
			{ 
			$dbh -> beginTransaction (); 
			$dbh->exec("SET time_zone = '+02:00';");
			$qry = "INSERT INTO `usrreg` (`user`, `region`, `active`) 
			VALUES ('".
					$user."','".
					$reg."','0')";
			////var_dump($form);
			
			$dbh->exec($qry);
			$lastId=$dbh->lastInsertId();

			$dbh->commit();
			
			}
			catch ( Exception $e )
			{ 
				$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
			} 
			$dbh=null;
		}
		return 1;
		return $lastId;
	}
	
public static function unsetCashier($user){
		
		if ($dbh=UsrDept::startConnection())
		{
			try
			{ 
			$dbh -> beginTransaction (); 
			$dbh->exec("SET time_zone = '+02:00';");
			$qry = "DELETE FROM `usrreg` WHERE user = $user";
			////var_dump($form);
			
			$dbh->exec($qry);
			
			$dbh->commit();
			
			}
			catch ( Exception $e )
			{ 
				$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
			} 
			$dbh=null;
		}
		return 1;

	}
	
	
}