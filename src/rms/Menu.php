﻿<?php


class Menu 
{

	public static function startConnection(){
		try {
		$dbo = new PDO("mysql:host=mysql.hostinger.co.uk;dbname=u462245891_rms", 
		"u462245891_admin", '10pan050r0ga');
		return $dbo;
		} catch (PDOException $e) {
		return false;
		}
	}


	public static function getMenuCashier($uid=false){
		if (!$uid or !is_numeric($uid) or ($uid<1)) return false;
		$mydept=UsrDept::getMyActiveDept($uid);
		if ($db=Menu::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$arResult['iamactive']=UsrDept::iamActive($uid);
			
			if ($arResult['iamactive']) {
			$arResult['dept']=UsrDept::getFullDepartment($mydept['id']);
			$arResult['systems']=Systems::getSysByDept($mydept['id']);
			$arResult['dept']['manager']=UsrDept::getActiveManager(UsrDept::getMyActiveRegion($uid));
			
			$arResult['dept']['manager']=$arResult['dept']['manager']['name'];
			$arResult['mycash']=$arResult['dept']['current_cash'];
			$arResult['depts']=UsrDept::getDepartments();
			$arResult['zals']=$arResult['depts'];
			$arResult['expany']=Cash::getListExp($uid,1);
			$arResult['expfix']=Cash::getListExp($uid,0);
			$arResult['expall']=Cash::getCashierMovings($uid,$mydept['id']);			
			}
			
			$db=null;
			return $arResult;
		}else
			return false;
	}
	
	public static function getMenuPov($uid=false,$dept){
		if (!$uid or !is_numeric($uid) or ($uid<1)) return false;
		if ($db=Menu::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$arResult['depts']=UsrDept::getDepartments();
			$arResult['dept']=UsrDept::getFullDepartment($dept);
			$arResult['dept']['manager']=UsrDept::getActiveCashier($dept, "ROLE_MANAGER");
			$arResult['dept']['manager']=$arResult['dept']['manager']['name'];
			$arResult['mycash']=Cash::getPovCash($uid);

			
			$db=null;
			return $arResult;
		}else
			return false;
	}
	
	public static function getMenuManager($uid=false,$dept=0){
		
		if (!$uid or !is_numeric($uid) or ($uid<1)) return false;
			$arResult['dept']['manager']=UsrDept::getActiveCashier($dept, "ROLE_MANAGER");
			$arResult['dept']['manager']=$arResult['dept']['manager']['name'];

			$arResult['depts']=Regions::getMyDepts($uid);
			$arResult['dept']=UsrDept::getFullDepartment($dept);

			$arResult['mycash']=Cash::getManagerCash($uid);
			$arResult['zals']=$arResult['depts'];
			$arResult['expany']=Cash::getListExp($uid,1);
			$arResult['expfix']=Cash::getListExp($uid,0);
			$arResult['expall']=Cash::getManagerMovings($uid,$dept);
			$arResult['me']=$uid;
			if ($dept==0) $arResult['dept']=null;
			$db=null;
			return $arResult;

	}
	
	public static function getMenuBoss($uid=false){
		if (!$uid or !is_numeric($uid) or ($uid<1)) return false;

		$arResult['regs'] = Regions::getFullTree();
		return $arResult;
	}	

}