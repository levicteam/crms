﻿<?php


class Systems 
{

	public static function startConnection(){
		try {
		$dbo = new PDO("mysql:host=mysql.hostinger.co.uk;dbname=u462245891_rms", 
		"u462245891_admin", '10pan050r0ga');
		
		return $dbo;
		} catch (PDOException $e) {
		return false;
		}
	}
	
	
	public static function getSingleSys($id){
		if ($db=Systems::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT * FROM systems WHERE id = $id";
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult=$result;
			$db=null;
			return $arResult;
		}
		return false;
	}
	
	public static function getListSys(){
		if ($db=Systems::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT * FROM systems";
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}
		return false;
	}
	
	public static function pushSys($form){
		if ($db=Systems::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "INSERT INTO systems (`name`, `desc`) 
			VALUES ('".$form['name']."','".$form['desc']."')";
			$res=$db->exec($qry);
			//var_dump($qry); die();
			$db=null;
			return $arResult;
		}
		return false;
	}
	
	public static function getSysByDept($dept){
		if ($db=Systems::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT systems.id, sysdpt.id as sdid, systems.name, sysdpt.current_cash, sysdpt.dept FROM sysdpt inner join systems on systems.id = sysdpt.sys where sysdpt.dept = $dept";
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			//var_dump($arResult);
			return $arResult;
		}
		return false;
	}
	
	public static function pushMoney($form){
	if (!$form) return false;
		if ($dbh=Systems::startConnection())
		{
			try
			{ 

				$dbh -> beginTransaction (); 
			$dbh->exec("SET time_zone = '+02:00';");
			$qry = "INSERT INTO syscashmoving (`creation_time`,`createdby`, `sys` ,`department`, `cash`, `comment`, `in`) 
			VALUES ('".date("Y-m-d H:i:s")."','".$form['creator']."','".$form['sys']."','".$form['dept']."','".
			$form['cash']."','".$form['comment']."','".$form['in']."')";
			$res=$dbh->exec($qry);
			
			$lastId=$dbh->lastInsertId();
			$qry = "UPDATE sysdpt SET current_cash = current_cash + ".$form['cash']*$form['in']." 
			WHERE sys = ".$form['sys']." AND dept = ".$form['dept'];
			$res=$dbh->exec($qry);
			$dbh=null;
						}
			catch ( Exception $e )
			{ 
				$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
			} 
		}
		return $lastId;
	}

	
	public static function getMyNewReports($uid){
		if ($db=Systems::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT * FROM `sysexchangereports` 
			INNER JOIN `sysexchangerows` 
			ON sysexchangereports.id = sysexchangerows.reportid 
			WHERE sysexchangereports.mustpass = $uid and sysexchangereports.passedby < 0";
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}
		return false;
	}	

	public static function getExchRep($rep){
		if ($db=Systems::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT *,	
							(SELECT name FROM systems WHERE systems.id = sysexchangerows.sys) AS sysname 
							FROM `sysexchangerows` 
			WHERE sysexchangerows.reportid = $rep";
			//var_dump($qry);
			$res=$db->query($qry);
			if ($res) while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $result1[]=$result;
			
			/*$qry = "SELECT *, (SELECT name FROM users WHERE users.id = sysexchangereports.createdby) AS createdbyname,
							(SELECT name FROM users WHERE users.id = sysexchangereports.passedby) AS passedbyname,	
							(SELECT addr FROM department WHERE department.id = sysexchangereports.department) AS deptname	
							FROM `sysexchangereports`
			WHERE sysexchangereports.id = $rep";*/
			$qry = "SELECT *, (SELECT name FROM users WHERE users.id = cashierexchange.comeout) AS createdbyname,
							(SELECT name FROM users WHERE users.id = cashierexchange.comein) AS passedbyname,	
							(SELECT addr FROM department WHERE department.id = cashierexchange.dept) AS deptname	
							FROM `cashierexchange`
			WHERE cashierexchange.id = $rep";
			$res=$db->query($qry);
			if ($res) while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $result2=$result;
			$arResult['repo']=$result2;
			$arResult['rows']=$result1;
			$db=null;
			//var_dump($arResult);
			return $arResult;
		}
		return false;
	}	

	public static function pushSysExchange($form){
	if (!$form) return false;
		if ($dbh=Systems::startConnection())
		{
			try
			{ 

			$dbh -> beginTransaction (); 
			$dbh->exec("SET time_zone = '+02:00';");
			$qry="INSERT INTO `sysexchangereports` (`timestamp`,`createdby`, `mustpass`,  `department`, `comment`,`lastrep`) 
			VALUES ('".date("Y-m-d H:i:s")."','".$form['createdby']."', '".
			$form['createdby']."',  '".$form['dept']."', '".$form['comment']."','".$form['lastrep']."');";
			$res=$dbh->exec($qry);
			
			$lastId=$dbh->lastInsertId();
			$form['reportid']=$lastId;
			$addtocash=0;
			foreach ($form['sys'] as $item){
				if (isset($form['manual'][$item])) $m=1; else $m=0;
				$qry="INSERT INTO `sysexchangerows` (`reportid`, `sys`, `in`, `out`, `res`, `manual`, `reason`,`bal`,`people`) 
				VALUES ('".$form['reportid']."', '".
							$form['sys'][$item]."', '".
							$form['incash'][$item]."', '".
							$form['outcash'][$item]."', '".
							$form['rescash'][$item]."', '".
							$m ."', '".
							$form['reason'][$item]."', '".
							$form['bal'][$item]."', '".
							$form['people'][$item].
							"');";
				$addtocash+=$form['rescash'][$item];
				$res=$dbh->exec($qry);
				
				$qry="UPDATE sysdpt SET current_cash = current_cash - ".$form['rescash'][$item]." 
						WHERE dept = ".$form['dept']." AND sys = ".$form['sys'][$item];
				$res=$dbh->exec($qry);	
			}
			
			$qry="UPDATE usrreg SET blocked = 1 WHERE user = ".$form['createdby']." AND active = 1";
			$res=$dbh->exec($qry);
			
			$qry="UPDATE department SET current_cash = current_cash + ".$addtocash." WHERE id = ".$form['dept'];
			$res=$dbh->exec($qry);
			
			$dbh=null;
						}
			catch ( Exception $e )
			{ 
				$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
				return $ok;
			} 
		}
		return $lastId;
	}

	public static function getTotalSysExch($dept){
		if ($db=Systems::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT sysexchangereports.id, users.name AS uname, sysexchangereports.timestamp, SUM( `in` ) AS input, SUM( `out` ) AS output, SUM( res ) AS res
					FROM `sysexchangerows`
					INNER JOIN sysexchangereports ON sysexchangereports.id = sysexchangerows.reportid
					INNER JOIN users ON users.id = sysexchangereports.createdby
					WHERE sysexchangereports.department = $dept
					GROUP BY reportid";
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}
		return false;
	}

	public static function pushSysToDept($arSys,$dept){
	if (!$arSys) return false;
		if ($dbh=Systems::startConnection())
		{
			try
			{ 

			$dbh -> beginTransaction (); 
			$dbh->exec("SET time_zone = '+02:00';");
			
			foreach ($arSys as $item){
				$qry="INSERT INTO sysdpt (`sys`,`dept`,`current_cash`) VALUES ('$item','$dept','0')";
			$res=$dbh->exec($qry);	
			}
			
			
			$dbh->commit();
			$dbh=null;
						}
			catch ( Exception $e )
			{ 
				$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
				return $ok;
			} 
		}
		return $lastId;
	}

	public static function unsetSysFrom($sdid){

		if ($dbh=Systems::startConnection())
		{
			try
			{ 

			$dbh -> beginTransaction (); 
			$dbh->exec("SET time_zone = '+02:00';");
			
			
			$qry="DELETE FROM sysdpt WHERE id = $sdid";
			
			
			$dbh->exec($qry);	
			
			
			$dbh->commit();
			$dbh=null;
						}
			catch ( Exception $e )
			{ 
				$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
				return $ok;
			} 
		}
		return $lastId;
	}

		public static function updateSys($form){
		global $app;
		if ($dbh=Cash::startConnection())
		{
			try
			{ 
			$dbh -> beginTransaction (); 
			$dbh->exec("SET time_zone = '+02:00';");
			$qry = "UPDATE systems SET `name`='".$form['name'].
					"', `desc`='".$form['desc']."' WHERE id = '".$form['id']."'";
			$dbh->exec($qry);
			
			
			$dbh->commit();
			}
			catch ( Exception $e )
			{ 
				$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
			} 
			$dbh=null;
		}
		return $lastId;
	}		
	
			public static function getSys($id){
		if ($db=Cash::startConnection())
		{
		$sql="SELECT * FROM systems WHERE id = $id";
		$res=$db->query($sql);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) {
				return $item;
			}
		}
		return -1;
	}
}