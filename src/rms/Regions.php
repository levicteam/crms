﻿<?php


class Regions 
{

	public static function startConnection(){
		try {
		$dbo = new PDO("mysql:host=mysql.hostinger.co.uk;dbname=u462245891_rms", 
		"u462245891_admin", '10pan050r0ga');
		return $dbo;
		} catch (PDOException $e) {
		return false;
		}
	}



	
	public static function getChildren($parent){
		if ($db=Regions::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$sql="SELECT * FROM regions WHERE parent = $parent";
			$res=$db->query($sql);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}else
			return false;
	}

	public static function getDepts($reg){
		if ($db=Regions::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$sql="SELECT * FROM department WHERE region = $reg";
			$res=$db->query($sql);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}else
			return false;
	}

	public static function getFullTree(){
		if ($db=Regions::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$arResult=Regions::getChildren(0);
			foreach ($arResult as &$item){
				$item['children']=Regions::getChildren($item['id']);
				
				foreach($item['children'] as &$item_l2){
					$item_l2['children']=Regions::getChildren($item_l2['id']);
					
					if (is_array($item_l2['children'])) foreach($item_l2['children'] as &$item_l3){
					$item_l3['children']=Regions::getDepts($item_l3['id']);
					
					}
				}
			}
			return $arResult;
		}else
			return false;
	}
	
	public static function getMyRegs($uid){
		if ($db=Regions::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$sql="SELECT * FROM regions WHERE manager = $uid AND children = 0";
			$res=$db->query($sql);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) {
					$arResult[]=$result;
				}

			$db=null;
			return $arResult;
		}else
			return false;
	}

	
	public static function getMyDepts($uid){
		if ($db=Regions::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$sql="SELECT * FROM regions WHERE manager = $uid AND children = 0";
			$res=$db->query($sql);
			$filter="";
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) {
					
					$filter.="OR region = ".$result['id']." ";
					
				}
			$filter=substr($filter,2);
			$sql="SELECT * FROM department WHERE ".$filter;
			
			$res=$db->query($sql);
			if (!$res) return array();
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) {
					$arResult[]=$result;
				}
			//var_dump($sql);
			$db=null;
			return $arResult;
		}else
			return false;
	}

	public static function getManagers($reg=false){
		if ($db=Regions::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			
			$sql="SELECT users.* FROM  users  WHERE roles LIKE '%ROLE_MANAGER%'";
			//$sql="SELECT users.* FROM `manager` INNER JOIN users ON manager.userid = users.id WHERE manager.ispov<1";
			$res=$db->query($sql);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) {
					$arResult[]=$result;
				}

			$db=null;
			return $arResult;
		}else
			return false;
	}
	
	
	
	/*
		public static function getManagers($reg=false,$free=false){
		if (!$reg)
			{
				$filter=" AND usrreg.region=".$reg." ";
				
			}
		else $filter = "";
		if ($free)
			$free=" AND usrreg.active = 0 ";
		else $free="";
		if ($db=UsrDept::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");

			
			$qry = "SELECT users.* FROM 
					users INNER JOIN usrreg 
					ON users.id = usrreg.user 
					WHERE users.roles LIKE '%ROLE_MANAGER%' ".$filter." AND users.id != $me".$free;
					////var_dump($qry);
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}
		return false;
	}
	
	*/
	
	
	

	public static function pushReg($form){
	if (!$form) return false;
		if ($dbh=Regions::startConnection())
		{
			try
			{ 
			//var_dump($form);
				$dbh -> beginTransaction (); 
			$dbh->exec("SET time_zone = '+02:00';");
			$qry = "INSERT INTO regions (`name`, `parent` ,`manager`, `children`) 
			VALUES ('".$form['name']."','".$form['parent']."','".$form['manager']."','0')";
			//var_dump($qry);
			$res=$dbh->exec($qry);
			
			$lastId=$dbh->lastInsertId();
			
			$qry= "INSERT INTO usrreg (`user`, `region`,`active`) Values ('".$form['manager']."','".$lastId."','1')";
			$res=$dbh->exec($qry);
			$dbh=null;
						}
			catch ( Exception $e )
			{ 
				$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
			} 
		}
		return $lastId;
	}
	
	public static function getSingle($rid){
		if ($db=Regions::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$sql="SELECT regions. * , regions.parent AS parentid, users.name AS mname, (
SELECT regions.name
FROM regions
WHERE regions.id = parentid
) AS pname
FROM regions

INNER JOIN users ON regions.manager = users.id
WHERE users.roles LIKE '%ROLE_MANAGER%'
AND regions.id = $rid";
			$res=$db->query($sql);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) {
					$arResult=$result;
				}

			$db=null;
			return $arResult;
		}else
			return false;
	}

	public static function getParent($pid){
		if ($db=Regions::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$sql="SELECT regions. * FROM regions
WHERE regions.id = $pid";
			$res=$db->query($sql);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) {
					$arResult=$result;
				}

			$db=null;
			return $arResult;
		}else
			return false;
	}

	public static function getRegByUser($uid){
		if ($db=Regions::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$sql="SELECT regions. * FROM regions INNER JOIN usrreg ON usrreg.region = regions.id 
WHERE ussreg.user = $uid";
			$res=$db->query($sql);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) {
					$arResult=$result;
				}

			$db=null;
			return $arResult;
		}else
			return false;
	}
	public static function getRegByDept($uid){
		if ($db=Regions::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$sql="SELECT * FROM department WHERE id = $uid";
			$res=$db->query($sql);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) {
					$arResult=$result['region'];
				}

			$db=null;
			return $arResult;
		}else
			return false;
	}
}