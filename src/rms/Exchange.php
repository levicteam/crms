﻿<?php


class Exchange 
{

	public static function startConnection(){
		try {
		$dbo = new PDO("mysql:host=mysql.hostinger.co.uk;dbname=u462245891_rms", 
		"u462245891_admin", '10pan050r0ga');
		return $dbo;
		} catch (PDOException $e) {
		return false;
		}
	}

	public static function passExchange($repo=0, $passedby=0){
		if (!is_numeric($repo) or ($repo<1)) return false;
		if (!is_numeric($passedby) or ($passedby<1)) return false;
		if ($dbh=Exchange::startConnection())
		{
			try
			{ 
			
				$dbh -> beginTransaction (); 
				$dbh->exec("SET time_zone = '+02:00';");
				$myEx = Exchange::getExchange($repo);
				//var_dump($myEx);
				$updateReport="UPDATE cashierexchange SET pass_time = '".date("Y-m-d H:i:s")."' , passed = ".$passedby." WHERE id= ".$repo.";";
				//var_dump($updateReport); die();
				$dbh -> exec ($updateReport);
				$reg=UsrDept::getMyActiveRegion($myEx['comeout']);
				$updateUsrDpt1 = "UPDATE usrreg SET active = 0 WHERE region = ".$reg.
				" AND user = ".$myEx['comeout'];
				$updateUsrDpt2 = "UPDATE usrreg SET active = 1, blocked = 0 WHERE region  = ".$reg.
				" AND user = ".$myEx['comein'];
				$updateUsrDpt3 = "UPDATE department SET active_cashier = ".$myEx['comein'].", sysexchblock = 0,
									current_cash = (SELECT current_cash 
									FROM cashierexchange WHERE id = $repo)
									WHERE id = ".$myEx['dept'];
				$updateBal="UPDATE sysdpt SET current_cash = ( SELECT bal
						FROM sysexchangerows
						WHERE sysexchangerows.reportid = $repo
						AND sysexchangerows.sys = sysdpt.sys )
						WHERE sysdpt.dept =".$myEx['dept'];		
				//var_dump($updateBal); die();
				$dbh -> exec ($updateUsrDpt1); 
				$dbh -> exec ($updateUsrDpt2); 
				$dbh -> exec ($updateUsrDpt3); 
				$dbh -> exec($updateBal);
 


				
				$dbh -> rollBack (); 
				
				//$dbh -> commit (); 
				$ok=true;
			}
			catch ( Exception $e )
			{ 
				$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
			} 
			$dbh=null;
		}
		return $ok;
	}	
	public static function getExchange($id=false){
		if (!$id or !is_numeric($id) or ($id<1)) return false;
		if ($db=Exchange::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT *,
			(
			SELECT name FROM users WHERE cashierexchange.comeout=users.id
			) as comeoutname,
			(
			SELECT name FROM users WHERE cashierexchange.comein=users.id
			) as comeinname 
			FROM cashierexchange WHERE id=$id";
			$res = $db->query($qry);
			//$res = $res->fetchAll(PDO::FETCH_ASSOC);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) 
			return $item;
			$db=null;
			//foreach ($res as $item) {return $item;}
			return false;
		}else
			return false;
	}
	
	
	public static function pushExchange($form=false, $creator=0){
		if (!$form) return false;
		if (!is_numeric($creator) or ($creator<1)) return false;
		if ($db=Exchange::startConnection())
		{
		try
			{ 

			$db -> beginTransaction (); 
			$db->exec("SET time_zone = '+02:00';");
			$qry = "INSERT INTO cashierexchange (`create_time`,
												`lastrep`,
												`comeout`, 
												`comein` ,
												`dept` ,
												`current_cash`, 
												`comment`) 
										VALUES ('".date("Y-m-d H:i:s")."',
												'".$form['lastrep']."',
												'".$creator."',
												'".$form['mustpass']."',
												'".$form['dept']."',
												'".$form['cash']."',
												'".$form['comment']."')";
			$res=$db->exec($qry);
			//var_dump($form);
			$lastId=$db->lastInsertId();
			$updateUsrDpt2 = "UPDATE usrreg SET blocked = 1 WHERE region  = 
				(SELECT * FROM department WHERE id = '".$form['dept']."')
				AND user = ".$myEx['comein'];
			$dbh -> exec ($updateUsrDpt2); 
			$form['reportid']=$lastId;
			$addtocash=0;
			foreach ($form['sys'] as $item){
				if (isset($form['manual'][$item])) $m=1; else $m=0;
				$qry="INSERT INTO `sysexchangerows` (`reportid`, `sys`, `in`, `out`, `res`, `manual`, `reason`,`bal`,`people`) 
				VALUES ('".$form['reportid']."', '".
							$form['sys'][$item]."', '".
							$form['incash'][$item]."', '".
							$form['outcash'][$item]."', '".
							$form['rescash'][$item]."', '".
							$m ."', '".
							$form['reason'][$item]."', '".
							$form['bal'][$item]."', '".
							$form['people'][$item].
							"');";
				$addtocash+=$form['rescash'][$item];
				$res=$db->exec($qry);
				}	
				$qry="UPDATE sysdpt SET current_cash = current_cash - ".$form['rescash'][$item]." 
						WHERE dept = ".$form['dept']." AND sys = ".$form['sys'][$item];
				$res=$db->exec($qry);				
				
				$qry="UPDATE cashierexchange SET current_cash = current_cash + ".$form['rescash'][$item]." 
						WHERE id = ".$lastId;
				$res=$db->exec($qry);				
			
				$db->commit();
			
			}
			catch ( Exception $e )
			{ 
				$db -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
				return $ok;
			} 			
			
			$db=null;
		}
		return $lastId;
	}	
	
	
	
	
	public static function getIncomingExchange($uid=false){
		if (!$uid or !is_numeric($uid) or ($uid<1)) return false;
		if ($db=Exchange::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			/*$qry = "SELECT * , (

			SELECT addr
			FROM department
			WHERE department.id = cashierexchange.dept
			) AS deptname, (

			SELECT name
			FROM users
			WHERE cashierexchange.comeout = users.id
			) AS comeoutname, (

			SELECT name
			FROM users
			WHERE cashierexchange.comein = users.id
			) AS comeinname
			FROM cashierexchange
			WHERE comein = $uid
			AND passed <0";
			**/
			$qry="SELECT * FROM cashierexchange
			WHERE comein = $uid
			AND passed <0";
			
			//var_dump($qry);
			$res = $db->query($qry);
			
			//$res = $res->fetchAll(PDO::FETCH_ASSOC);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) 
			if  ($item) $result=$item['id'];
			
			$db=null;
			
			//foreach ($res as $item) {return $item;}
			if ($result) return Systems::getExchRep($result); else return false;
		}else
			return false;			
	}	
	
	public static function getLastSysRep($dept){
		//if (!$uid or !is_numeric($uid) or ($uid<1)) return false;
		if ($db=Exchange::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT *
					FROM cashierexchange
					WHERE `create_time` = (
					SELECT max( `create_time` ) AS mt
					FROM cashierexchange
					WHERE dept = $dept )
					AND dept = $dept ";
			
			
			$res = $db->query($qry);
			
			//$res = $res->fetchAll(PDO::FETCH_ASSOC);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) 
			return $item;
			$db=null;
			//foreach ($res as $item) {return $item;}
			
		}else
			return -1;			
	}
	
	
	
	
	public static function getShortPerortList($dept=false){
		if (!$dept or !is_numeric($dept) or ($dept<1)) return false;
		if ($db=Exchange::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT cashierexchange.id, cashierexchange.current_cash, users.name AS uname, cashierexchange.create_time, SUM( `in` ) AS input, SUM( `out` ) AS output, SUM( res ) AS res
			FROM `sysexchangerows`
			INNER JOIN cashierexchange ON cashierexchange.id = sysexchangerows.reportid
			INNER JOIN users ON users.id = cashierexchange.comeout
			WHERE cashierexchange.dept = $dept
			GROUP BY reportid";
			
			//var_dump($qry);
			$res = $db->query($qry);
			$arResult=array();
			//$res = $res->fetchAll(PDO::FETCH_ASSOC);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) 
			$arResult[]=$item;
			$db=null;
			//foreach ($res as $item) {return $item;}
			return $arResult;
		}else
			return -1;			
	}		
	
}