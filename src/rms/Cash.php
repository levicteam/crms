﻿<?php


class Cash 
{

	public static function startConnection(){
		try {
		$dbo = new PDO("mysql:host=mysql.hostinger.co.uk;dbname=u462245891_rms", 
		"u462245891_admin", '10pan050r0ga');
		
		return $dbo;
		} catch (PDOException $e) {
		return false;
		}
	}
	
	public static function stopConnection(){
		
	}
	
	public static function getAllSuperadmin(){
		
		return 0;
	}
	
	public static function getDepartmentCashMovings($dept=false,
													$passed='all',
													$creator=false,
													$mustpass=false){
		//if (!$dept or !is_numeric($dept) or ($dept<1)) return false;
		if ($db=Cash::startConnection())
		{	
			$db->exec("SET time_zone = '+02:00';");
			$filter = ' WHERE ';
			switch ($passed){
				case 'all': $filter.=" passedby>-2 "; break;
				case 'passed': $filter.=" passedby>0 "; break;
				case 'new': $filter.=" passedby=-1 "; break;
				default: $filter.=" passedby>-2 "; break;
			}

			////var_dump($creator);
			////var_dump($mustpass);
			
			if (is_numeric($dept)and ($dept>0)) $filter.=" and department='$dept'";
			if (is_numeric($creator)and ($creator>0)) $filter.=" and createdby='$creator'";
			if (is_numeric($mustpass)and ($mustpass>0)) $filter.=" and mustpass='$mustpass'";

			$qry = "SELECT cashmoving.id,cashmoving.cash, cashmoving.creation_time, department, 
			cashmoving.createdby as createdbyid,  cashmoving.passedby as passedbyid,
			(
			SELECT name FROM users WHERE cashmoving.createdby=users.id
			) as createdby,
			(
			SELECT name FROM users WHERE cashmoving.passedby=users.id
			) as passedby,
			(
			SELECT addr FROM department WHERE cashmoving.department=department.id
			) as dname
			FROM cashmoving  ".$filter;
			////var_dump($qry);
			$res = $db->query($qry);
			$arList=array();
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) 
			$arList[]=$item;
			$db=null;
			////var_dump($arList);
			foreach ($arList as &$item){
				if ($item['passedby']==null) $item['passedby']=0;
				
			}
			return $arList;
		}else
			return false;
	}
	
	public static function getSingle($id=false){
		if (!$id or !is_numeric($id) or ($id<1)) return false;
		if ($db=Cash::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT *,
			(
			SELECT name FROM users WHERE cashmoving.createdby=users.id
			) as createdbyname ,
			(
			SELECT name FROM users WHERE cashmoving.passedby=users.id
			) as passedbyname 
			FROM cashmoving WHERE id=$id";
			$res = $db->query($qry);
			//$res = $res->fetchAll(PDO::FETCH_ASSOC);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) 
			return $item;
			$db=null;
			//foreach ($res as $item) {return $item;}
			return false;
		}else
			return false;
	}
	
	public static function pushSingle($form=false, $creator=0){
		if (!$form) return false;
		if (!is_numeric($creator) or ($creator<1)) return false;
		if ($db=Cash::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "INSERT INTO cashmoving (`creation_time`,`createdby`, `mustpass` ,`department`, `cash`, `in`,`comment`) 
			VALUES ('".date("Y-m-d H:i:s")."','".$creator."','".$form['mustpass']."','".$form['dept']."','".
					$form['cash']."','".$form['in']."','".$form['comment']."')";
			$res=$db->exec($qry);
			
			$lastId=$db->lastInsertId();
			
			$db=null;
		}
		return $lastId;
	}
	
	public static function passSingle($repo=0, $passedby=0, $dept=0, $mov=0){
		global $app;
		if (!is_numeric($repo) or ($repo<1)) return false;
		if (!is_numeric($passedby) or ($passedby<1)) return false;
		if ($dbh=Cash::startConnection())
		{
			try
			{ 
			$dbh->exec("SET time_zone = '+02:00';");
				$dbh -> beginTransaction (); 
				
				$getCash="SELECT * FROM cashmoving WHERE id = ".$repo.";";
				$res=$dbh->query($getCash);
				while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult=$result;
				$repoCash=$arResult['cash'];
				$updateReport="UPDATE cashmoving SET pass_time = '".date("Y-m-d H:i:s")."', 
									
									passedby = ".$passedby." WHERE id= ".$repo.";";

				$updateDept="UPDATE department SET current_cash = current_cash + ".$repoCash*$mov." WHERE id = ".$dept;
				$dbh -> exec ($updateReport); 
				
				$dbh -> exec ($updateDept); 
				
				if ($app['security']->isGranted('ROLE_POV') or $app['security']->isGranted('ROLE_MANAGER')) 
						$user=$arResult['mustpass'];
				elseif ($app['security']->isGranted('ROLE_LOCALUSER'))
						$user=$arResult['createdby'];
						
					
				$updatePov="UPDATE manager SET mycash = mycash + ".$repoCash*(-$mov)." WHERE userid = ".$user;
				$dbh -> exec ($updatePov); 
				
				$dbh -> commit (); 
				$ok=true;
			}
			catch ( Exception $e )
			{ 
				$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
			} 
			$dbh=null;
		}
		return $ok;
	}
	

	
	public static function getMyCash($uid){
		if ($db=Cash::startConnection())
		{	
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT * FROM department,users,usrdpt 
					WHERE usrdpt.user = users.id 
					AND usrdpt.dept = department.id
					AND usrdpt.user = ".$uid." 
					AND usrdpt.active = 1";
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			////var_dump($arResult);
			return $arResult[0]['current_cash'];
		}
		return false;			
	}
	
	public static function getPovCash($uid){
		if ($db=Cash::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT manager.* FROM manager 
					INNER JOIN users 
					ON users.id = manager.userid 
					WHERE users.id = $uid AND ispov = 1";
					
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult=$result;
			$db=null;
			return $arResult['mycash'];
		}
		return false;		
	}
	
	public static function getManagerCash($uid){
		if ($db=Cash::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT manager.* FROM manager 
					INNER JOIN users 
					ON users.id = manager.userid 
					WHERE users.id = $uid AND ispov = 0";
					
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult=$result;
			$db=null;
			return $arResult['mycash'];
		}
		return false;		
	}
	
	public static function doExpenses($form){
		global $app;
		if ($dbh=Cash::startConnection())
		{
			try
			{ 
			//$dbh -> beginTransaction (); 
			$dbh->exec("SET time_zone = '+02:00';");
			$qry = "INSERT INTO expenses (`timestamp`,`userid`, `dept` ,`type`, `cash`, `comment`) 
					VALUES ( '".date("Y-m-d H:i:s")."','".
					$form['userid']."','".
					$form['dept']."','".
					$form['type']."','".
					$form['cash']."','".
					$form['comment']."')";
			$dbh->exec($qry);
			$lastId=$dbh->lastInsertId();
			if (($app['security']->isGranted('ROLE_MANAGER') or $app['security']->isGranted('ROLE_POV')) and 
			(!$app['security']->isGranted('ROLE_LOCALUSER')))
			$qry = "UPDATE manager SET mycash = mycash - ".$form['cash']." WHERE userid = ".$form['userid'];
			elseif ($app['security']->isGranted('ROLE_LOCALUSER'))
			$qry = "UPDATE department SET current_cash = current_cash - ".$form['cash']." WHERE id = ".$form['dept'];
			//var_dump($qry);
			$dbh->exec($qry);
			//$dbh->commit();
			}
			catch ( Exception $e )
			{ 
				//$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
			} 
			$dbh=null;
		}
		return $lastId;
	}
	
	public static function getTypeExps(){
		if ($db=Cash::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			$qry = "SELECT * FROM expenses_types WHERE id > 0";
			$res=$db->query($qry);
			while ($result=$res->fetch(PDO::FETCH_ASSOC)) 
				if ($result) $arResult[]=$result;
			$db=null;
			return $arResult;
		}
		return false;	
	}


	public static function getSingleExp($id=false,$any=false){
		if (!$id or !is_numeric($id) or ($id<1)) return false;
		if ($db=Cash::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			if ($any==0)
			$qry = "SELECT expenses . * , expenses_types.expenses, department.addr
					FROM expenses
					INNER JOIN expenses_types ON expenses.type = expenses_types.id
					INNER JOIN department ON department.id = expenses.dept
					WHERE expenses.id =$id";
			elseif ($any==1)
			$qry = "SELECT expenses . * 
					FROM expenses
					
					WHERE expenses.id =$id";				
			$res = $db->query($qry);
			////var_dump($qry);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) {
				////var_dump($item);
				////var_dump($item);
			return $item;}
			$db=null;
			//foreach ($res as $item) {return $item;}
			return false;
		}else
			return false;
	}

	public static function getListExp($uid,$any=false){
		if ($db=Cash::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			if (!$any)
			$qry = "SELECT expenses . * , expenses_types.expenses, department.addr AS addr, DATE_FORMAT(timestamp, '%d.%m.%Y %H:%i') AS dttm
					FROM expenses
					INNER JOIN expenses_types ON expenses.type = expenses_types.id
					INNER JOIN department ON department.id = expenses.dept
					WHERE expenses.userid=$uid";				
			elseif ($any==0)
			$qry = "SELECT expenses . * , expenses_types.expenses, department.addr AS addr, DATE_FORMAT(timestamp, '%d.%m.%Y %H:%i') AS dttm
					FROM expenses
					INNER JOIN expenses_types ON expenses.type = expenses_types.id
					INNER JOIN department ON department.id = expenses.dept
					WHERE expenses.userid=$uid";				
			elseif ($any==1)
			$qry = "SELECT expenses . * , users.name, DATE_FORMAT(timestamp, '%d.%m.%Y %H:%i') AS dttm
					FROM expenses INNER JOIN users ON users.id = expenses.userid
					WHERE expenses.userid=$uid";			
			
			$res = $db->query($qry);
			////var_dump($qry);
			$arResult=array();
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) {
				////var_dump($item);
				////var_dump($item);
			$arResult[]=$item;}
			$db=null;
			//foreach ($res as $item) {return $item;}
			return $arResult;
		}else
			return false;
	}

	public static function getList($dept){
		if (!$dept or !is_numeric($dept) or ($dept<1)) return false;
		if ($db=Cash::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			
			$qry = "SELECT cashmoving.*, users.id AS uid, users.name, cashmoving.cash*cashmoving.in AS moved FROM cashmoving INNER JOIN users 
			ON cashmoving.createdby = users.id
			WHERE department = $dept";
							
			$res = $db->query($qry);
			//$res = $res->fetchAll(PDO::FETCH_ASSOC);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) 
			$arResult[]=$item;
			$db=null;
			//foreach ($res as $item) {return $item;}
			return $arResult;
		}else
			return false;		
	}
	
	public static function getListSys($dept){
		if (!$dept or !is_numeric($dept) or ($dept<1)) return false;
		if ($db=Cash::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");
			
			$qry = "SELECT syscashmoving . * , users.id AS uid, users.name, 
			syscashmoving.cash * syscashmoving.in AS moved, systems.name AS sysname
FROM syscashmoving
INNER JOIN users ON syscashmoving.createdby = users.id
INNER JOIN systems ON syscashmoving.sys = systems.id
WHERE department = $dept";
							
			$res = $db->query($qry);
			//$res = $res->fetchAll(PDO::FETCH_ASSOC);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) 
			$arResult[]=$item;
			$db=null;
			//foreach ($res as $item) {return $item;}
			return $arResult;
		}else
			return false;		
	}	
	
	public static function getAllMovings($dept=false){
		if ($db=Cash::startConnection())
		{
			if (!$dept){
				$sql1="SELECT * FROM v_big_cashmoving";
				$sql2="SELECT * FROM v_big_expenses";
			}else{
				$sql1="SELECT * FROM v_big_cashmoving WHERE dept = $dept";
				$sql2="SELECT * FROM v_big_expenses WHERE dept = $dept";				
			}
			
			
			$arResult=array();
			$res=$db->query($sql1);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) 
			$arResult[]=$item;
			$res=null;
			$res=$db->query($sql2);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) 
			$arResult[]=$item;
			return $arResult;
		}	
	}
	
	public static function getAllExp($dept=false){
		if ($db=Cash::startConnection())
		{
			if ($dept>0){
				//$sql1="SELECT * FROM v_big_cashmoving";
				$sql2="SELECT * FROM v_big_expenses WHERE dept = $dept";
			}else{
				//$sql1="SELECT * FROM v_big_cashmoving WHERE dept = $dept";
				$sql2="SELECT * FROM v_big_expenses";				
			}
			
			
			$arResult=array();
			$res=$db->query($sql2);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) 
			$arResult[]=$item;
			/*$res=null;
			$res=$db->query($sql2);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) 
			$arResult[]=$item;*/
			return $arResult;
		}	
	}
	
	public static function getManagerMovings($manager,$dept=0){
		$table="_manager";
		$cashier=false;
		if ($dept>0){
				if ($manager<1) {$manager=UsrDept::getActiveCashier($dept, "ROLE_LOCALUSER");
				
				$manager=$manager['id'];}
				$table="";
			}
		if ($db=Cash::startConnection())
		{
			$filter = "";

			if ($manager>0 and $dept<1){
				$filter.=" WHERE (userid = $manager OR mustpass = $manager) ";	
				//	$filter.=" WHERE 1 ";				
			}
			if ($dept>0){
				//$filter.=" AND dept = $dept ";
				$filter.=" WHERE dept = $dept ";
			}
			
				$sql="SELECT * FROM (SELECT * FROM v_big_cashmoving$table
						UNION ALL
						SELECT * FROM v_big_expenses) as tmptbl ".
						$filter." ORDER BY tmst DESC";	
						////var_dump($sql);
			$arResult=array();
			$res=$db->query($sql);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) {
				//if ($dept>0) $item['cash']*=-1;
				$arResult[]=$item;
			}
			//var_dump($arResult);
			$db=null;
			return $arResult;
		}	
		return -1;
	}
	
	public static function getCashierStartTime($cashier){
		if ($db=Cash::startConnection())
		{
		$sql="SELECT max( pass_time ) AS pstm, passed
FROM cashierexchange
WHERE comein = $cashier";
$res=$db->query($sql);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) {
				return $item['pstm'];
			}
		}
		return -1;
	}
	
	
	
	public static function getCashierMovings($cashier,$dept=0,$fromdate=false){
		if ($db=Cash::startConnection())
		{
			$filter = "";
			if ($cashier>0){
				$filter.=" WHERE (userid = $cashier) ";				
			}
			if ($dept>0){
				$filter.=" AND dept = $dept ";				
			}
			if ($fromdate){
				$filter.=" AND tmst > '".Cash::getCashierStartTime($cashier)."' ";
			}
				$sql="SELECT * FROM (SELECT * FROM v_big_cashmoving
						UNION ALL
						SELECT * FROM v_big_expenses) as tmptbl ".
						$filter." ORDER BY tmst DESC";	
						//var_dump($sql);
			$arResult=array();
			$res=$db->query($sql);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) {
				//if ($item['cash']>0) $item['cash']*=-1;
				$arResult[]=$item;
			}
			////var_dump($arResult);
			return $arResult;
		}	
	}
	
	
	public static function pushExpType($form){
		global $app;
		if ($dbh=Cash::startConnection())
		{
			try
			{ 
			$dbh -> beginTransaction (); 
			$dbh->exec("SET time_zone = '+02:00';");
			$qry = "INSERT INTO expenses_types (`expenses`, `expdesc`) 
					VALUES ('".
					$form['name']."','".
					$form['desc']."')";
			$dbh->exec($qry);
			$lastId=$dbh->lastInsertId();
			
			$dbh->commit();
			}
			catch ( Exception $e )
			{ 
				$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
			} 
			$dbh=null;
		}
		return $lastId;
	}	

		public static function getExpById($id){
		if ($db=Cash::startConnection())
		{
		$sql="SELECT * FROM expenses_types WHERE id = $id";
		$res=$db->query($sql);
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) {
				return $item;
			}
		}
		return -1;
	}
	public static function updateExpType($form){
		global $app;
		if ($dbh=Cash::startConnection())
		{
			try
			{ 
			$dbh -> beginTransaction (); 
			$dbh->exec("SET time_zone = '+02:00';");
			$qry = "UPDATE expenses_types SET `expenses`='".$form['name'].
					"', `expdesc`='".$form['desc']."' WHERE id = '".$form['id']."'";
			$dbh->exec($qry);
			
			
			$dbh->commit();
			}
			catch ( Exception $e )
			{ 
				$dbh -> rollBack (); 
				echo "Шеф! Фсё пропало : " . $e -> getMessage (); 
				$ok=false;
			} 
			$dbh=null;
		}
		return $lastId;
	}	

	public static function getListExpBoss($dept){
		if ($db=Cash::startConnection())
		{
			$db->exec("SET time_zone = '+02:00';");

			
							$qry="SELECT *, DATE_FORMAT(tmst, '%d.%m.%Y %H:%i') AS tm FROM (SELECT * FROM v_big_expenses
						UNION ALL
						SELECT * FROM v_big_fromsys) as tmptbl  WHERE dept=$dept ORDER BY tmst DESC";	
			
			
			//$qry="SELECT *, DATE_FORMAT(tmst, '%d.%m.%Y %H:%i') AS tm FROM `v_big_expenses` WHERE dept=$dept ORDER BY tmst DESC";
			/*
			$qry = "SELECT expenses . * , expenses_types.expenses AS exptype, department.addr AS addr, 
				DATE_FORMAT(timestamp, '%d.%m.%Y %H:%i') AS tmst, users.name AS created 
					FROM expenses
					INNER JOIN expenses_types ON expenses.type = expenses_types.id
					INNER JOIN department ON department.id = expenses.dept
					INNER JOIN users ON expenses.userid = users.id 
					WHERE expenses.dept=$dept";				
			*/
			
			$res = $db->query($qry);

			$arResult=array();
			while ($item=$res->fetch(PDO::FETCH_ASSOC)) {

			$arResult[]=$item;}
			$db=null;

			return $arResult;
		}else
			return false;
	}

	
}