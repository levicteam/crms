<?php

$app->get('/reports/all', function() use ($app) {
	$repo=Cash::getDepartmentCashMovings(false,'all');
	return $app['twig']->render('moving-cash-list.twig', array('reports'=>$repo));
});

// просмотр формы передачи по айди
$app->get('/reports/view-{rid}', function($rid) use ($app) {
	$repo=Cash::getSingle($rid);
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
	$repo['my']=($user==$repo['createdby'])?true:false;
	////var_dump($repo);
	$repo['action']='/index.php/reports/pass';
	if ($app['security']->isGranted('ROLE_MANAGER')) {
		$menu=Menu::getMenuManager($user,0);
		
	}elseif ($app['security']->isGranted('ROLE_LOCALUSER')){
		$menu=Menu::getMenuCashier($user);
	}elseif ($app['security']->isGranted('ROLE_POV')) {
		$menu=Menu::getMenuManager($user,0);
		$depts=UsrDept::getDepartments();
		$menu['dept']=null;	
		
	}	
	/*
	$menu=array('mycash'=>$mycash,
				'zals'=>$depts,
				'zal'=>$current_dept,
				'iamactive'=>1
				);	
	*/
	return $app['twig']->render('moving-cash-report.twig', array_merge($repo,$menu));
});

// список неутвержденных форм
$app->get('/reports/notpassed', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
	if ($app['security']->isGranted('ROLE_MANAGER')) {
		$menu=Menu::getMenuManager($user,0);
	}elseif ($app['security']->isGranted('ROLE_POV')) {
		$mycash=Cash::getPovCash($user);
		$depts=UsrDept::getDepartments();
		$menu=Menu::getMenuManager($user,0);
		$menu['dept']=null;
	}elseif ($app['security']->isGranted('ROLE_LOCALUSER')){
		$menu=Menu::getMenuCashier($user);
		$current_dept = UsrDept::getMyActiveDept($user);
		$mycash=$current_dept['current_cash'];
		$depts=UsrDept::getDepartments();

	}	
	$form['reports']=Cash::getDepartmentCashMovings(false,'new',false,$user);
	
	return $app['twig']->render('notpassed-list.twig', array_merge($menu,$form));
});

// список моих отчетов
$app->get('/reports/myreports', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
	$repo=Cash::getDepartmentCashMovings(false,'all',$user,false);
	return $app['twig']->render('notpassed-list.twig', array('reports'=>$repo));
});

$app->get('/reports/createin/{zal}', function($zal) use ($app){
	$token = $app['security']->getToken();
	$menu=array();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
	if ($app['security']->isGranted('ROLE_LOCALUSER') and 
	!$app['security']->isGranted('ROLE_MANAGER') and 
	!$app['security']->isGranted('ROLE_POV') and 
	!$app['security']->isGranted('ROLE_BOSS') ){
		$menu=Menu::getMenuCashier($user);
		$targetUsers=UsrDept::getActivePov();
		$activeDept = UsrDept::getMyActiveDept($user);
		//$targetUsers[]=UsrDept::getActiveCashier($activeDept['id'],"ROLE_MANAGER");
		
		$targetUsers[]=UsrDept::getActiveManager(UsrDept::getMyActiveRegion($user));
		$outcoming=1;
		$targetUser=0;
	}elseif ($app['security']->isGranted('ROLE_MANAGER')) {
		$menu=Menu::getMenuManager($user,$zal);
		
		$targetUser=UsrDept::getActiveCashier(UsrDept::getRegByDept($zal));
		
		$targetUsers=array();
		//var_dump($targetUser);
		$outcoming=0;
	}elseif ($app['security']->isGranted('ROLE_POV')) {
		$targetUser=UsrDept::getActiveCashier($zal);
		$current_dept=UsrDept::getFullDepartment($zal);
		$outcoming=0;
		$menu=Menu::getMenuManager($user,$zal);		
		
	}
	
	$repoform=array('now'=>date("Y-m-d H:i:s"),
					'action'=>'/index.php/reports/createin', 
					'targetuser'=>$targetUser, 
					'targetdepartment'=>$current_dept,
					'targetusers'=>$targetUsers,
					'outcoming'=>$outcoming,
					);
	
	return $app['twig']->render('moving-cash-form.twig', array_merge($menu,$repoform));
});

$app->post('/reports/createin', function() use ($app){
	$message = $app['request'];
	$form['cash']=$message->get('cash');
	$form['mustpass']=$message->get('mustpass');
	$form['comment']=$message->get('comment');
	$form['dept']=$message->get('dept');
	if ($app['security']->isGranted('ROLE_LOCALUSER') and 
	!$app['security']->isGranted('ROLE_MANAGER') and 
	!$app['security']->isGranted('ROLE_POV') and 
	!$app['security']->isGranted('ROLE_BOSS') ) $form['in']=-1;
	else $form['in']=1;
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
	if ($lastId=Cash::pushSingle($form,$user)) 
		return $app->redirect('/index.php/reports/view-'.$lastId);
});

$app->post('/reports/pass', function() use ($app){
	$message = $app['request'];
	if ($message->get('passed')==1){
		$repo=$message->get('repid');
		$dept=$message->get('dept');
		$token = $app['security']->getToken();
		if (null !== $token) {
			$user = $token->getUser()->getId();
			}
	if ($app['security']->isGranted('ROLE_MANAGER')) {
		$mov=-1;
	}elseif ($app['security']->isGranted('ROLE_POV')) {
		$mov=-1;
	}elseif ($app['security']->isGranted('ROLE_LOCALUSER')){
		$mov=1;
	}
		if (Cash::passSingle($repo,$user,$dept,$mov)) 
			return $app->redirect('/index.php/reports/view-'.$repo);		
	}
	return "Ошибка утверждения формы передачи";
});


$app->get('/systems/addmoney/{zal}-{sys}', function($zal,$sys) use ($app){
	$token = $app['security']->getToken();
	$menu=array();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
	if ($app['security']->isGranted('ROLE_MANAGER')){
		$activeDept = UsrDept::getFullDepartment($zal);
		////var_dump($activeDept);
		$outcoming=1;
		$targetUser=0;
				$current_dept=$activeDept;
		$mycash=$current_dept['current_cash'];

	$menu=Menu::getMenuManager($user,$zal);
	}
	$sys=Systems::getSingleSys($sys);
	
	$repoform=array('now'=>date("Y-m-d H:i:s"),
					'action'=>'/index.php/systems/addmoney', 
					'targetdepartment'=>$current_dept,
					'outcoming'=>$outcoming,
					'system'=>$sys,
					);
	return $app['twig']->render('sys-addmoney.twig', array_merge($menu,$repoform));
});

$app->get('/systems/extractmoney/{zal}-{sys}', function($zal,$sys) use ($app){
	$token = $app['security']->getToken();
	$menu=array();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
	if ($app['security']->isGranted('ROLE_MANAGER')){
		$activeDept = UsrDept::getFullDepartment($zal);
		////var_dump($activeDept);
		$outcoming=1;
		$targetUser=0;
				$current_dept=$activeDept;
		$mycash=$current_dept['current_cash'];
				$menu=array('mycash'=>$mycash,
				'zals'=>$depts,
				'zal'=>$current_dept,
				'iamactive'=>true);
	}
	$sys=Systems::getSingleSys($sys);
	
	$repoform=array('now'=>date("Y-m-d H:i:s"),
					'action'=>'/index.php/systems/addmoney', 
					'targetdepartment'=>$current_dept,
					'outcoming'=>$outcoming,
					'system'=>$sys,
					);
	return $app['twig']->render('sys-extractmoney.twig', array_merge($menu,$repoform));
});

$app->post('/systems/addmoney', function() use ($app){
	$message = $app['request'];
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}	
	$form['creator']=$user;
	$form['cash']=$message->get('cash');
	$form['sys']=$message->get('sys');
	$form['comment']=$message->get('comment');
	$form['dept']=$message->get('dept');
	$form['in']=$message->get('in');
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
	if ($lastId=Systems::pushMoney($form)) 
		return $app->redirect('/index.php/manager/dept-'.$form['dept']);
});


$app->get('/reports/cash/{dept}', function($dept) use ($app) {
	$repo=Cash::getList($dept);
	////var_dump($repo);
	$token = $app['security']->getToken();
		if (null !== $token) {
		$user = $token->getUser()->getId();
		}	
	if ($app['security']->isGranted('ROLE_LOCALUSER') and 
	!$app['security']->isGranted('ROLE_MANAGER') and 
	!$app['security']->isGranted('ROLE_POV') and 
	!$app['security']->isGranted('ROLE_BOSS')){
		$menu=Menu::getMenuCashier($user);
	}
	elseif ($app['security']->isGranted('ROLE_MANAGER')){
		$menu=Menu::getMenuManager($user,$dept);
	}
		
		
					$current_dept=UsrDept::getMyActiveDept($user);
		$mycash=$current_dept['current_cash'];
	$repoform=array('now'=>date("Y-m-d H:i:s"),
					'action'=>'/index.php/systems/addmoney', 
					'targetuser'=>$targetUser, 
					'targetdepartment'=>$current_dept,
					'targetusers'=>$targetUsers,
					'outcoming'=>$outcoming,
					'system'=>$sys,
					'zals'=>UsrDept::getDepartments()
					);

	return $app['twig']->render('repolist.twig', array_merge(array('reports'=>$repo),$menu));
});


$app->get('/reports/sys/{dept}', function($dept) use ($app) {
	$repo=Cash::getListSys($dept);
	////var_dump($repo);
	$token = $app['security']->getToken();
		if (null !== $token) {
		$user = $token->getUser()->getId();
		}	
	if ($app['security']->isGranted('ROLE_LOCALUSER') and 
	!$app['security']->isGranted('ROLE_MANAGER') and 
	!$app['security']->isGranted('ROLE_POV') and 
	!$app['security']->isGranted('ROLE_BOSS') ){
		$menu=Menu::getMenuCashier($user);
	}
	elseif ($app['security']->isGranted('ROLE_MANAGER')){
		$menu=Menu::getMenuManager($user,$dept);
	}

	
	return $app['twig']->render('repolistsys.twig', array_merge(array('reports'=>$repo),$menu));
});

$app->get('/reports/sysxchg/{dept}', function($dept) use ($app) {
	$repo=Exchange::getShortPerortList($dept);
	////var_dump($repo);
	$token = $app['security']->getToken();
		if (null !== $token) {
		$user = $token->getUser()->getId();
		}	
	if ($app['security']->isGranted('ROLE_LOCALUSER') and 
	!$app['security']->isGranted('ROLE_MANAGER') and 
	!$app['security']->isGranted('ROLE_POV') and 
	!$app['security']->isGranted('ROLE_BOSS') 
	){
		$menu=Menu::getMenuCashier($user);
	}
	elseif ($app['security']->isGranted('ROLE_MANAGER')){
		$menu=Menu::getMenuManager($user,$dept);
	}

	
	return $app['twig']->render('exchlistsys.twig', array_merge(array('reports'=>$repo),$menu));
});






