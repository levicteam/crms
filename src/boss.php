<?php

$app->get('/boss', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}		
	
	$menu = Menu::getMenuBoss($user['id']);
	
	$data=array();
	
	return $app['twig']->render('start-boss.twig', array_merge($menu,$data));
});



$app->get('/boss/dept-{id}', function($id) use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
	$menu = Menu::getMenuBoss($user['id']);
	$data=array();
	$data['dept']=UsrDept::getFullDepartment($id);
//var_dump(Regions::getRegByDept($id));
	$data['systems']=Systems::getSysByDept($id);
	$data['cashiers']=UsrDept::getUsersOfRegion('ROLE_LOCALUSER',0,Regions::getRegByDept($id));
	$data['reports']=Exchange::getShortPerortList($id);
	$data['expall']=Cash::getListExpBoss($id);
	//var_dump($data['exch']);
	return $app['twig']->render('zal-boss.twig', array_merge($menu,$data));
});

$app->get('/boss/deptexp-{id}', function($id) use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
	$menu = Menu::getMenuBoss($user['id']);
	$data=array();
	$data['dept']=UsrDept::getFullDepartment($id);
//var_dump(Regions::getRegByDept($id));
	$data['systems']=Systems::getSysByDept($id);
	$data['cashiers']=UsrDept::getUsersOfRegion('ROLE_LOCALUSER',0,Regions::getRegByDept($id));
	return $app['twig']->render('zal-boss.twig', array_merge($menu,$data));
});

