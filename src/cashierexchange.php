<?php

$app->get('/exchange/view-{rid}', function($rid) use ($app) {
	$repo=Exchange::getExchange($rid);
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
		
	if ($app['security']->isGranted('ROLE_LOCALUSER') and 
	!$app['security']->isGranted('ROLE_MANAGER') and 
	!$app['security']->isGranted('ROLE_POV') and 
	!$app['security']->isGranted('ROLE_BOSS') ){
		$menu=Menu::getMenuCashier($user);
	}
	elseif ($app['security']->isGranted('ROLE_MANAGER')){
		$menu=Menu::getMenuManager($user);
	}
	elseif ($app['security']->isGranted('ROLE_BOSS')){
		$menu = Menu::getMenuBoss($user);
	}

	$repo['my']=($user==$repo['comeout'])?true:false;
	////
	$repo['sys']=Systems::getExchRep(Exchange::getLastSysRep($repo['dept'])['id']);
	//var_dump($repo);
	//var_dump($repo['sys']);
	$repo['action']='/index.php/exchange/pass';
		$iamactive=UsrDept::iamActive($user);

	$repo['targetusers']=$targetUsers; 

	$tmp=Systems::getExchRep($rid);
	$repo=array_merge($repo,$tmp);
	$repo['repo']['total']=0;
	foreach($tmp['rows'] as $item) $repo['repo']['total']+=$item['res'];
	

	return $app['twig']->render('exchange-cashier-report.twig', array_merge($repo,$menu));
});


$app->post('/exchange/pass', function() use ($app){
	$message = $app['request'];
	if ($message->get('passed')==1){
		$repo=$message->get('repid');
		$token = $app['security']->getToken();
		if (null !== $token) {
			$user = $token->getUser()->getId();
			}
		if (Exchange::passExchange($repo,$user)) 
			return $app->redirect('/index.php/exchange/view-'.$repo);		
	}
	return "Ошибка утверждения формы передачи";
});

$app->get('/exchange/pass', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();

		}
	if (!$repo=Exchange::getIncomingExchange($user)) $fail=true;
	//var_dump($repo);
	$menu=Menu::getMenuCashier($user);
	//$menu['iamactive']=UsrDept::iamActive($user);

	$repo['my']=($user==$repo['comeout'])?true:false;
	
	$repo['action']='/index.php/exchange/pass';
	if ($fail) return $app['twig']->render('null-universal.twig', 
		array_merge(array('type'=>'warning','mes1'=>'Нечего принимать!','mes2'=>''),$repo,$menu));
	return $app['twig']->render('exchange-cashier-report.twig', array_merge($repo,$menu));
});

$app->get('/exchange/do', function() use ($app){

	
		////var_dump($targetUsers);
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
	
	$menu=Menu::getMenuCashier($user);
	$target_reg = UsrDept::getMyActiveRegion($user);
	$targetUsers=UsrDept::getUsersOfRegion('ROLE_LOCALUSER',$user,$target_reg,true);	
		$current_dept = UsrDept::getMyActiveDept($user);
		$current_dept = UsrDept::getFullDepartment($current_dept['id']);
		$iamactive=UsrDept::iamActive($user);
		$iamblocked=UsrDept::iamBlocked($user);
	$data1=array('now'=>date("Y-m-d H:i:s"),
	'action'=>'/index.php/exchange/do', 
	'targetusers'=>$targetUsers);
	$data2=array('now'=>date("Y-m-d H:i:s"),
	'preexch'=>Exchange::getLastSysRep($current_dept['id'])['create_time'],
	'iamactive'=>$iamactive,
	'sysdone'=>$iamblocked,
	'zal'=>$current_dept,
	'systems'=>Systems::getSysByDept($current_dept['id']),
	'targetdepartment'=>$current_dept,
	'targetusers'=>$targetUsers,
	);
	return $app['twig']->render('exchange-cashier-form.twig',array_merge($menu,$data2,$data1) );
});

$app->get('/exchange/sysdo', function() use ($app){

	
		////var_dump($targetUsers);
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
		$current_dept = UsrDept::getMyActiveDept($user);
		$current_dept = UsrDept::getFullDepartment($current_dept['id']);
		$targetUsers=UsrDept::getUsersOfDepartment('ROLE_LOCALUSER',$user);	
		$iamactive=UsrDept::iamActive($user);
		$iamblocked=UsrDept::iamBlocked($user);
	$menu=Menu::getMenuCashier($user);

	return $app['twig']->render('sys-report-form.twig', array_merge(array('now'=>date("Y-m-d H:i:s"),
	'preexch'=>Exchange::getLastSysRep($current_dept['id'])['timestamp'],
	'action'=>'/index.php/exchange/sysdo', 
	'targetusers'=>$targetUsers, 
	'iamactive'=>$iamactive,
	'mycash'=>$mycash,
	'sysdone'=>$iamblocked,
	'zal'=>$current_dept,
	'systems'=>Systems::getSysByDept($current_dept['id']),
	'targetdepartment'=>$current_dept,
	'targetusers'=>$targetUsers,
	),$menu));
});

$app->post('/exchange/do', function() use ($app){
	$message = $app['request'];
	$form['cash']=$message->get('willbecash');
	$form['mustpass']=$message->get('mustpass');
	$form['comment']=$message->get('comment');
	$form['dept']=$message->get('dept');
	
	$form['sys']=$message->get('sys');
	$form['incash']=$message->get('incash');
	$form['outcash']=$message->get('outcash');
	$form['manual']=$message->get('manual');
	$form['rescash']=$message->get('rescash');
	$form['people']=$message->get('people');
	$form['reason']=$message->get('reason');
	$form['bal']=$message->get('willbe');
	
	$form['lastrep']=$message->get('lasttime');
	$form['createdby']=$user;
	
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
	if ($lastId=Exchange::pushExchange($form,$user)) 
		return $app->redirect('/index.php/exchange/view-'.$lastId);
});

/*
$app->post('/exchange/sysdo', function() use ($app){
	$message = $app['request'];
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
	$form['sys']=$message->get('sys');
	$form['incash']=$message->get('incash');
	$form['outcash']=$message->get('outcash');
	$form['manual']=$message->get('manual');
	$form['rescash']=$message->get('rescash');
	$form['people']=$message->get('people');
	$form['reason']=$message->get('reason');
	$form['bal']=$message->get('willbe');
	$form['dept']=$message->get('dept');
	$form['comment']=$message->get('comment');
	$form['lastrep']=$message->get('lasttime');
	$form['createdby']=$user;
	$form['mustpass']=$message->get('mustpass');
	
	if ($lastId=Systems::pushSysExchange($form)) 
		return $app->redirect('/index.php/exchange/sysxch-'.$lastId);
	else return "Any error";
});*/

$app->post('/exchange/pass', function() use ($app){
	$message = $app['request'];
	if ($message->get('passed')==1){
		$repo=$message->get('repid');
		$token = $app['security']->getToken();
		if (null !== $token) {
			$user = $token->getUser()->getId();
			}
		if (Exchange::passExchange($repo,$user)) 
			return $app->redirect('/index.php/exchange/view-'.$repo);		
	}
	return "Ошибка утверждения формы переснены";
});