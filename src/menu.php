<?php
$app->get('/pov/dept-{id}', function($id) use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
		
	$menu=Menu::getMenuManager($user['id'],$id);

	return $app['twig']->render('zal-pov.twig', $menu);
});


$app->get('/manager/dept-{id}', function($id) use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
	$menu=Menu::getMenuManager($user['id'],$id);
	//echo "ololo 1 ";
	$systems=Systems::getSysByDept($id);
	$menu2['systems']=$systems;
	$menu2['allexp']=Cash::getManagerMovings($user['id'],$id);
	//echo "ololo 1 ";
	$menu2['cashiers']=UsrDept::getUsersOfRegion('ROLE_LOCALUSER',$user['id']);
	//echo "ololo 1 ";
	return $app['twig']->render('zal-manager.twig', array_merge($menu,$menu2));
});
// кассир
$app->get('/cashier', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}
		
	$iamactive=UsrDept::iamActive($user['id']);


	if ($iamactive){

		$menu=Menu::getMenuCashier($user['id']);
		$menu['expall']=Cash::getCashierMovings($user['id'],0,1);
		////var_dump($menu['expenses']);
	}

	if ($iamactive)
	return $app['twig']->render('zal-localuser.twig', $menu);
	else 	return $app['twig']->render('index.twig', array(
									'iamactive'=>$iamactive,
									'zal'=>$current_dept,
									'mycash'=>$mycash,
									'systems'=>$systems,));
});
// менеджер зала
$app->get('/manager', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}

	$menu=Menu::getMenuManager($user['id'],0);
	
//$menu['allexp']=null;
	
	return $app['twig']->render('index.twig', array_merge($menu));
});
// поверенный
$app->get('/pov', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}
		
	$depts=UsrDept::getDepartments();
	////var_dump($depts);
	$etypes=Cash::getTypeExps();
	////var_dump($etypes);
	$mycash=Cash::getPovCash($user['id']);
	$action='/index.php/pov/createexp';
	return $app['twig']->render('index.twig', array(
									'zals'=>$depts,
									'zal'=>$current_dept,
									'mycash'=>$mycash,
									'systems'=>$systems,
									'cashiers'=>$dept_cashiers));
});



