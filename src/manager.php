<?php

$app->get('/manager/newdept', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}
		$myreg=UsrDept::getMyActiveRegion($user['id']);
	$menu=Menu::getMenuManager($user['id'],0);
	$menu['cashiers']=UsrDept::getUsersOfRegion('ROLE_LOCALUSER',$user['id'],false,true);
	$menu['systems']=Systems::getListSys();
	$menu['region']=$myreg;
	$menu['action']="/index.php/manager/newdept";
//$menu['allexp']=null;
	
	return $app['twig']->render('zal-new.twig', array_merge($menu));
});

$app->post('/manager/newdept', function() use ($app){
	$message = $app['request'];
	if ($message->get('addr'))
	{
		$form['addr']=$message->get('addr');
		$form['systems']=$message->get('systems');
		$form['cash']=$message->get('startcash');
		$form['region']=$message->get('region');
		$form['desc']=$message->get('desc');
		$form['sysexchblock']=1;
		$form['cashier']=$message->get('cashier');
		$token = $app['security']->getToken();
		if (null !== $token) {
			$user = $token->getUser()->getId();
			}
		if ($dept=UsrDept::pushDept($form)) 
				
			return $app->redirect('/index.php/manager');
	
	}
	return "Ошибка отправки данных";
});


$app->get('/manager/usersofreg', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
	$menu=Menu::getMenuManager($user['id']);

	$menu2['cashiers']=UsrDept::getUsersOfRegion('ROLE_LOCALUSER',$user['id'],false);
	//var_dump($menu2);
	return $app['twig']->render('reg-cashiers-list.twig', array_merge($menu,$menu2));
});

$app->get('/manager/setcashier', function() use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
	$menu=Menu::getMenuManager($user['id']);

	$menu2['cashiers']=UsrDept::getFreeUsers();
	
	return $app['twig']->render('free-cashiers-list.twig', array_merge($menu,$menu2));
});

$app->get('/manager/unsetcashier/{id}', function($id) use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
	$menu=Menu::getMenuManager($user['id']);
	UsrDept::unsetCashier($id);
	//$menu2['cashiers']=UsrDept::getFreeUsers();
	
	return $app->redirect('/index.php/manager/usersofreg');
});

$app->get('/manager/delsys/{sdid}-{dept}', function($sdid,$dept) use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
	Systems::unsetSysFrom($sdid);

			return $app->redirect('/index.php/manager/sysdept/'.$dept);

});

$app->get('/manager/sysdept/{dept}', function($dept) use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
	$menu=Menu::getMenuManager($user['id']);
	$data['systems']=Systems::getSysByDept($dept);
	$data['newsys']=Systems::getListSys();
	$data['tdept']=$dept;
	$data['action']="/index.php/manager/sysadd";
	//var_dump($data);
	return $app['twig']->render('sys-editor-manager.twig', array_merge($menu,$data));
});

$app->post('/manager/sysadd', function() use ($app){
	$message = $app['request'];
	if ($message->get('dept'))
	{
		$form['dept']=$message->get('dept');
		$form['sys'][]=$message->get('newsystem');
		Systems::pushSysToDept($form['sys'],$form['dept']);
		
			return $app->redirect('/index.php/manager/sysdept/'.$form['dept']);
	
	}
	return "Ошибка отправки данных";
});


$app->get('/manager/passcashier/{id}', function($id) use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
	$menu=Menu::getMenuManager($user['id']);

	UsrDept::setCashier($id,UsrDept::getMyActiveRegion($user['id']));
	
	return $app->redirect('/index.php/manager/usersofreg');
});
