<?php
use Silex\Application;
use Silex\Provider;
$app = new Application();
$app->register(new Provider\DoctrineServiceProvider());
$app->register(new Provider\SecurityServiceProvider());
$app->register(new Provider\RememberMeServiceProvider());
$app->register(new Provider\SessionServiceProvider());
$app->register(new Provider\ServiceControllerServiceProvider());
$app->register(new Provider\UrlGeneratorServiceProvider());
$app->register(new Provider\TwigServiceProvider());
$app->register(new Provider\SwiftmailerServiceProvider());

// Register the SimpleUser service provider.
$simpleUserProvider = new SimpleUser\UserServiceProvider();
$app->register($simpleUserProvider);
$app->mount('/user', $simpleUserProvider);
require_once __DIR__.'/../src/Demo/User.php';
//require_once __DIR__.'/../src/SimpleUser/User.php';



$app['twig.path'] = array(__DIR__.'/../templates');
$app['user.options'] = array(
    'templates' => array(
        'layout' => 'layout.twig',
        'register' => 'register.twig',
        'register-confirmation-sent' => 'register-confirmation-sent.twig',
        'login' => 'login.twig',
        'login-confirmation-needed' => 'login-confirmation-needed.twig',
        'forgot-password' => 'forgot-password.twig',
        'reset-password' => 'reset-password.twig',
        'view' => 'view.twig',
        'edit' => 'edit.twig',
        'list' => 'list.twig',
    ),
    'mailer' => array('enabled' => false),
    'editCustomFields' => array('twitterUsername' => 'Twitter username'),
    'userClass' => '\Demo\User',
	'cache' => __DIR__.'/../var/cache/twig',
);


//$app['security.role_hierarchy'] = array(
//    'ROLE_LOCALUSER' => array(),
//    'ROLE_ADMIN' => array('ROLE_LOCALUSER','ROLE_POV','ROLE_MANAGER' ),
//'ROLE_ADMIN' => array('ROLE_LOCALUSER','ROLE_POV','ROLE_BOSS','ROLE_OFFICE','ROLE_MANAGER' ),
//);

$app['security.firewalls'] = array(
    /*
    // Ensure that the login page is accessible to all
    'login' => array(
        'pattern' => '^/user/login$',
    ),*/
    'secured_area' => array(
        'pattern' => '^.*$',
        'anonymous' => true,
        'remember_me' => array(),
        'form' => array(
            'login_path' => '/user/login',
            'check_path' => '/user/login_check',
        ),
        'logout' => array(
            'logout_path' => '/user/logout',
        ),
        'users' => $app->share(function($app) { return $app['user.manager']; }),
    ),

);

$app['security.access_rules'] = array(
	
    array('^/reports*', 'IS_AUTHENTICATED_FULLY'),
    array('^/systems*', 'IS_AUTHENTICATED_FULLY'),
	//array('^/reports/createin', 'IS_AUTHENTICATED_FULLY'),
	//array('^/reports/notpassed', 'IS_AUTHENTICATED_FULLY'),
	//array('^/reports/pass', 'IS_AUTHENTICATED_FULLY'),
	//array('^/reports/view-*', 'IS_AUTHENTICATED_FULLY'),
	array('^/pov*', 'ROLE_POV'),
	array('^/cashier*', 'ROLE_LOCALUSER'),
	array('^/manager*', 'ROLE_MANAGER'),
	//array('^/user*', 'IS_AUTHENTICATED_FULLY'),
    array('^.*$', 'IS_AUTHENTICATED_ANONYMOUSLY')
);


$app['user.passwordStrengthValidator'] = $app->protect(function(SimpleUser\User $user, $password) {
    if (strlen($password) < 4) {
        return 'Password must be at least 4 characters long.';
    }
    if (strtolower($password) == strtolower($user->getName())) {
        return 'Your password cannot be the same as your name.';
    }
});





$app['debug'] = true;
require_once __DIR__ . '/../config/prod.php';
require_once __DIR__.'/../src/rms/Cash.php';
require_once __DIR__.'/../src/rms/Exchange.php';
require_once __DIR__.'/../src/rms/UsrDept.php';
require_once __DIR__.'/../src/rms/Systems.php';
require_once __DIR__.'/../src/rms/Menu.php';
require_once __DIR__.'/../src/rms/Regions.php';
date_default_timezone_set('Europe/Kiev');
//var_dump(date("Y-m-d H:i:s"));

$app->before(function () use ($app){
	//$token = $app['security']->getToken();
	//if (null !== $token) {
	//	$user = $token->getUser()->getId();
	//	}	
	//$app['iamblocked']=UsrDept::iamBlocked($user);
	return "";
});

return $app;
