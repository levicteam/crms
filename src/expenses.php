<?php
$app->get('/createexp/{zal}', function($zal) use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
		
	$depts=UsrDept::getDepartments();
	$etypes=Cash::getTypeExps();


	$iamactive=UsrDept::iamActive($user['id']);

	$action='/index.php/createexp';
	if ($app['security']->isGranted('ROLE_LOCALUSER') and 
	!$app['security']->isGranted('ROLE_MANAGER') and 
	!$app['security']->isGranted('ROLE_POV') and 
	!$app['security']->isGranted('ROLE_BOSS') and 
	
	$iamactive) {


		$menu=Menu::getMenuCashier($user['id']);
		$menu2=array(	'now'=>date("Y-m-d H:i:s"),
						'action'=>$action,
						'user'=>$user, 
						'zals'=>$depts,
						'etypes'=>$etypes);

	}
		elseif( $app['security']->isGranted('ROLE_MANAGER')){
		
		$menu=Menu::getMenuManager($user['id'],$zal);
		
		$managercash=Cash::getManagerCash($user['id']);
				$menu2=array(	'now'=>date("Y-m-d H:i:s"),
						'action'=>$action,
						'user'=>$user, 
						'zals'=>UsrDept::getDepartments(),
						'etypes'=>$etypes);
	}

	return $app['twig']->render('doexp-form.twig',array_merge($menu,$menu2));
});

$app->get('/createexp/{zal}/any', function($zal) use ($app) {
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
		
	$depts=UsrDept::getDepartments();
	$etypes=Cash::getTypeExps();


	$iamactive=UsrDept::iamActive($user['id']);

	$action='/index.php/createexp/any';
	if ($app['security']->isGranted('ROLE_LOCALUSER') and 
	!$app['security']->isGranted('ROLE_MANAGER') and 
	!$app['security']->isGranted('ROLE_POV') and 
	!$app['security']->isGranted('ROLE_BOSS') and 
	
	$iamactive) {


		$menu=Menu::getMenuCashier($user['id']);
		$menu2=array(	'now'=>date("Y-m-d H:i:s"),
						'action'=>$action,
						'user'=>$user, 
						'zals'=>$depts,
						'manager_cash'=>$managercash,
						'etypes'=>$etypes);

	}
	elseif( $app['security']->isGranted('ROLE_MANAGER') and $iamactive){
		
		$menu=Menu::getMenuManager($user['id'],$zal);
		
		$managercash=Cash::getManagerCash($user['id']);
				$menu2=array(	'now'=>date("Y-m-d H:i:s"),
						'action'=>$action,
						'user'=>$user, 
						'zals'=>UsrDept::getDepartments(),
						'etypes'=>$etypes);
	}	
	
		return $app['twig']->render('doexpany-form.twig',array_merge($menu,$menu2));
	return $app['twig']->render('doexpany-form.twig', array(
									'now'=>date("Y-m-d H:i:s"),
									'action'=>$action,
									'mycash'=>$mycash,
									'iamactive'=>$iamactive,
									'user'=>$user, 
									'dept'=>$current_dept,
									'zals'=>$depts,
									'zal'=>$current_dept,
									'manager_cash'=>$managercash,
									'etypes'=>$etypes));
});


$app->post('/createexp', function() use ($app){
	$message = $app['request'];
	$form['cash']=$message->get('cash');
	$form['type']=$message->get('type');
	$form['comment']=$message->get('comment');
	$form['dept']=$message->get('dept');
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
	$form['userid']=$user;
	if ($lastId=Cash::doExpenses($form)) 
		if ($app['security']->isGranted('ROLE_MANAGER'))
		return $app->redirect('/index.php/manager/exp-'.$lastId.'-1');
		elseif ($app['security']->isGranted('ROLE_LOCALUSER'))
		return $app->redirect('/index.php/cashier/exp-'.$lastId.'-1');
});

$app->post('/createexp/any', function() use ($app){
	$message = $app['request'];
	$form['cash']=$message->get('cash');
	$form['type']=0;
	$form['comment']=$message->get('comment');
	$form['dept']=$message->get('dept');
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user = $token->getUser()->getId();
		}
	$form['userid']=$user;
	if ($lastId=Cash::doExpenses($form)) 
		if ($app['security']->isGranted('ROLE_POV'))
		return $app->redirect('/index.php/pov/exp-'.$lastId);
		elseif ($app['security']->isGranted('ROLE_MANAGER'))
		return $app->redirect('/index.php/manager/exp-'.$lastId.'-0');
		elseif ($app['security']->isGranted('ROLE_LOCALUSER'))
		return $app->redirect('/index.php/cashier/exp-'.$lastId.'-0');
		});




$app->get('/cashier/exp-{rid}-{fix}', function($rid,$fix) use ($app){
	if ($fix==1) $repo=Cash::getSingleExp($rid,0);
	elseif ($fix==0) $repo=Cash::getSingleExp($rid,1);
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
	$depts=UsrDept::getDepartments();
	$menu=Menu::getMenuCashier($user['id']);
	////var_dump($menu);
	if ($fix==1)
	return $app['twig']->render('exp-report.twig', array_merge($repo,$menu));
else
	return $app['twig']->render('exp-any-report.twig', array_merge($repo,$menu));
});

$app->get('/manager/exp-{rid}-{fix}', function($rid,$fix) use ($app){
	if ($fix==1) $repo=Cash::getSingleExp($rid,0);
	elseif ($fix==0) $repo=Cash::getSingleExp($rid, 1);
	$token = $app['security']->getToken();
	if (null !== $token) {
		$user['id'] = $token->getUser()->getId();
		$user['name'] = $token->getUser()->getName();
		}	
	$menu=Menu::getMenuManager($user['id'],0);

	if ($fix==1)
	return $app['twig']->render('exp-report.twig', array_merge($repo,$menu));
else
	return $app['twig']->render('exp-any-report.twig', array_merge($repo,$menu));
});