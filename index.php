<?php
ini_set('date.timezone', 'Europe/Kiev');
use Symfony\Component\Debug\Debug;

require_once __DIR__.'/../vendor/autoload.php';

require_once __DIR__.'/src/app.php';


/**
Главная
*/
$app->get('/', function () use ($app) {

	if ($app['security']->isGranted('ROLE_MANAGER')){
		return $app->redirect('/index.php/manager');
	}elseif ($app['security']->isGranted('ROLE_POV')){
		return $app->redirect('/index.php/pov');
	}elseif ($app['security']->isGranted('ROLE_ADMIN')){
		return $app->redirect('/index.php/admin');
	}elseif ($app['security']->isGranted('ROLE_BOSS')){
		return $app->redirect('/index.php/boss');
	}elseif ($app['security']->isGranted('ROLE_LOCALUSER')) {
		return $app->redirect('/index.php/cashier'); 
	}else
		
	return $app['twig']->render('index.twig', array());
	

    
});




require_once __DIR__.'/src/cashmoving.php';
require_once __DIR__.'/src/cashierexchange.php';
require_once __DIR__.'/src/expenses.php';
require_once __DIR__.'/src/menu.php';
require_once __DIR__.'/src/admin.php';
require_once __DIR__.'/src/boss.php';
require_once __DIR__.'/src/manager.php';


$app->get('/view/incominglist/all', function() use ($app) {
	
require_once __DIR__.'/src/ajax/ssp.class.php';	
$table = 'v_cashmoving';
 
// Table's primary key
$primaryKey = 'id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'id', 'dt' => 0,
        'formatter' => function( $d, $row ) {
            return "<a href=\"/index.php/reports/view-$d\">".$d."</a>";
        } ),
    array( 'db' => 'creation_time',  'dt' => 1 ),
    array( 'db' => 'department',   'dt' => 2 ),
    array( 'db' => 'createdby',     'dt' => 3 ),
    array( 'db' => 'passedby',     'dt' => 4 ),
);



// SQL server connection information
$sql_details = array(
    'user' => $app['db.options']['user'],
    'pass' => $app['db.options']['password'],
    'db'   => $app['db.options']['dbname'],
    'host' => $app['db.options']['host']
);
 
 
return json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);
});

$app->run();
