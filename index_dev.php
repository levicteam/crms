<?php

use Symfony\Component\Debug\Debug;

require_once __DIR__.'/../vendor/autoload.php';

Debug::enable();

require_once __DIR__ . '/config/prod.php';
require_once __DIR__.'/src/app.php';


$app->mount('/user', $simpleUserProvider);
$app->get('/', function () use ($app) {
	
	
	        try{  
	$a = new PDO("mysql:host=". $app['db.options']['host'] .";dbname=".
                                $app['db.options']['dbname'], $app['db.options']['user'], 
								$app['db.options']['password']);
         }
        catch(PDOException $e)
        {
            printme(' подключиться к MySQL не получилось', 1);
            printme(' проверьте настройки в коде скрипта (поля класса DBO этого скрипта), а также убедитесь что PHP PDO включено | текст ошибки:');
            printme("Error: ".$e->getMessage());
            exit(); 
        }
         
	
	
	
	

    return $app['twig']->render('index.twig', array());
});

$app->get('/reports', function() {
	return $app['twig']->render('repolist.twig', array());
});

$app->get('/reports/{department}/{date}', function() {
	return $app['twig']->render('reposingle.twig', array());
});


$app->run();